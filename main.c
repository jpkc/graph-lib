#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct {
	int *adjacent_vertex_array;
	unsigned int adjacent_vertex_array_size, adjacent_vertex_array_last;
	} Vertex;

typedef struct {
	unsigned int vertex_from, vertex_to;
	int vertex_weight;
	} Edge;

typedef struct {
	Edge *e;
	unsigned int e_size, e_last;

	Vertex *v;
	unsigned int v_size;

	} Graph;

int EdgeComp(const void *a, const void *b) {
	if(((Edge*)a)->vertex_from == ((Edge*)b)->vertex_from) {
		return (((Edge*)a)->vertex_to - ((Edge*)b)->vertex_to);
	}
	return (((Edge*)a)->vertex_from - ((Edge*)b)->vertex_from);
}

Edge EdgeCreateRand(int max_vertex, int max_weight) {
	Edge edge;
	edge.vertex_from = rand()%max_vertex;
	edge.vertex_to = rand()%max_vertex;
	edge.vertex_weight = (rand()%(2*max_weight + 1))-max_weight;
	return edge;
}

int IntComp(const void *a, const void *b) {
	return (*(int*)a - *(int*)b);
}

void ImprimeVertice(Vertex *v) {
	unsigned int i;
	for(i = 0; i < v->adjacent_vertex_array_last; i++) {
		if(i > 0) {
			printf(", ");
		}
		printf("%u", v->adjacent_vertex_array[i]);
	}
	printf("\n");
}

void EdgePrint(Edge *e) {
	printf("%u -> %u: %d\n", e->vertex_from, e->vertex_to, e->vertex_weight);
}

void GraphPrint(Graph *g) {
	unsigned int i;
	printf("Grafo:\n----------------------------------------\n\tEdges:\n");
	for(i = 0; i < g->e_last; i++) {
		printf("\t\t%u: ", i);
		EdgePrint(g->e + i);
	}
	printf("\tVertexes:\n");
	for(i = 0; i < g->v_size; i++) {
		printf("\t\tVertex %3d: ", i);
		ImprimeVertice(&(g->v[i]));
	}
	printf("\n----------------------------------------\n");
}

Vertex VertexCreate() {
	Vertex v;
	v.adjacent_vertex_array_size = 16;
	v.adjacent_vertex_array = (int *) malloc(sizeof(int)*v.adjacent_vertex_array_size);
	v.adjacent_vertex_array_last = 0;
	return v;
}

void VertexAddAdjacent(Vertex *v, int adjacent_vertex) {
	if(v->adjacent_vertex_array_last >= v->adjacent_vertex_array_size) {
		v->adjacent_vertex_array_size *= 2;
		v->adjacent_vertex_array = (int *) realloc(v->adjacent_vertex_array, sizeof(int) * v->adjacent_vertex_array_size);
	}
	v->adjacent_vertex_array[v->adjacent_vertex_array_last] = adjacent_vertex;
	v->adjacent_vertex_array_last++;
}

Graph *GraphCreate() {
	Graph *g = (Graph *) malloc(sizeof(Graph));
	g->e_last = 0;
	g->e_size = 64;
	g->e = (Edge *) malloc(sizeof(Edge)*g->e_size);

	g->v_size = 0;
	g->v = NULL;

	return g;
}

void GraphEdgeAdd(Graph *g, Edge e) {
	if(g->e_last >= g->e_size) {
		g->e_size *= 2;
		g->e = (Edge *) realloc(g->e, sizeof(Edge) * g->e_size);
	}
	g->e[g->e_last] = e;
	g->e_last++;
}

void GraphSortEdges(Graph *g) {
	qsort (g->e, g->e_last, sizeof(Edge), EdgeComp);
}

void DigraphEdgesToGraphEdges(Graph *g) {
	unsigned int i;
	unsigned int edge_count = g->e_last;

	Edge e;
	for(i = 0; i < edge_count; i++) {
		e.vertex_from = g->e[i].vertex_to;
		e.vertex_to = g->e[i].vertex_from;
		e.vertex_weight = g->e[i].vertex_weight;
		GraphEdgeAdd(g, e);
	}
}

void GraphRemoveRepeatedEdges(Graph *g) {
	unsigned int i, j;
	if(g->e_last > 0) {
		for(j = 0, i = 1; i < g->e_last;) {
			if(EdgeComp(&(g->e[j]), &(g->e[i]))) {
				g->e[++j] = g->e[i];
			}
			i++;
		}
		g->e_last = j + 1;
	}
}

void GraphRemoveLoopEdges(Graph *g) {
	unsigned int i;
	for(i = 0; i < g->e_last;) {
		if(g->e[i].vertex_from == g->e[i].vertex_to) {
			g->e[i] = g->e[g->e_last--];
		}
		else
			i++;
	}
}

void GraphUpdateVertexVectorSize(Graph *g) {
	unsigned int i;
	g->v_size = 0;
	for(i = 0; i < g->e_size; i++) {
		if(g->v_size < g->e[i].vertex_from)
			g->v_size = g->e[i].vertex_from;
		if(g->v_size < g->e[i].vertex_to)
			g->v_size = g->e[i].vertex_to;
	}
	g->v_size++;
}

void GraphCreateVertexesAdjacentLists(Graph *g) {
	unsigned int i;

	if(g->v != NULL)	{
		/* TODO: Do we already have a Graph? Free allocated memory. */
	}

	g->v = (Vertex *) malloc(sizeof(Vertex)*g->v_size);
	for (i = 0; i < g->v_size; i++)
		g->v[i] = VertexCreate();

	for(i = 0; i < g->e_last; i++) {
		VertexAddAdjacent(&(g->v[g->e[i].vertex_from]), g->e[i].vertex_to);
	}
}

void DiGraphPrepare(Graph *g) {
	GraphRemoveLoopEdges(g);

	GraphSortEdges(g);
	GraphRemoveRepeatedEdges(g);
	GraphUpdateVertexVectorSize(g);
	GraphCreateVertexesAdjacentLists(g);
}

void GraphPrepare(Graph *g) {
	GraphRemoveLoopEdges(g);

	DigraphEdgesToGraphEdges(g);

	GraphSortEdges(g);
	GraphRemoveRepeatedEdges(g);
	GraphUpdateVertexVectorSize(g);
	GraphCreateVertexesAdjacentLists(g);
}

void GraphTraceback(Graph *g, unsigned int *visited_vertex_vector, unsigned int destination_vertex) {
	unsigned int i, min_level_adjacent_vertex;

	if(visited_vertex_vector[destination_vertex] != -1) {
		printf("%u", destination_vertex);

		while(visited_vertex_vector[destination_vertex] > 0) {
			min_level_adjacent_vertex = destination_vertex;
			for(i = 0; i < g->v[destination_vertex].adjacent_vertex_array_last; i++) {
				if(visited_vertex_vector[g->v[destination_vertex].adjacent_vertex_array[i]] < visited_vertex_vector[min_level_adjacent_vertex]) {
					min_level_adjacent_vertex = g->v[destination_vertex].adjacent_vertex_array[i];
				}
			}
			printf(" <- %u", min_level_adjacent_vertex);
			destination_vertex = min_level_adjacent_vertex;
		}
		printf("\n");
	}
}

int DFSRec(Graph *g, unsigned int origin_vertex, unsigned int destination_vertex, unsigned int *visited_vertex_vector) {
	unsigned int i;
	unsigned int current_vertex;

	for(i = 0; i < g->v[origin_vertex].adjacent_vertex_array_size; i++) {
		current_vertex = g->v[origin_vertex].adjacent_vertex_array[i];
		if(visited_vertex_vector[current_vertex] == -1) {
			visited_vertex_vector[current_vertex] = visited_vertex_vector[origin_vertex] + 1;
			if(current_vertex == destination_vertex) {
				printf("\tPath found: ");
				return 1;
			}
			if(DFSRec(g, current_vertex, destination_vertex, visited_vertex_vector)) {
				return 1;
			}
		}
	}
	return 0;
}

void DFS(Graph *g, unsigned int origin_vertex, unsigned int destination_vertex) {
	unsigned int i;
	int path_found = 0;
	unsigned int *visited_vertex_vector;

	printf("DFS: ");

	if(g->v_size < 1) {
		printf("Empty graph.\n");
		return;
	}

	if(origin_vertex < g->v_size && destination_vertex < g->v_size) {
		if(origin_vertex == destination_vertex) {
			printf("Trivial path found. Origin and destination vertex are the same: %u.\n", origin_vertex);
			return;
		}
		visited_vertex_vector = (unsigned int *) malloc(sizeof(unsigned int) * g->v_size);
		for(i = 0; i < g->v_size; i++) {
			visited_vertex_vector[i] = -1;
		}
		visited_vertex_vector[origin_vertex] = 0;
		path_found = DFSRec(g, origin_vertex, destination_vertex, visited_vertex_vector);

		if(path_found) {
			GraphTraceback(g, visited_vertex_vector, destination_vertex);
		}
		free(visited_vertex_vector);
	}
	else {
		if(origin_vertex > g->v_size) {
			printf("\n\tStarting point %u out or range. Graph vertex span: 0 to %u.\n", origin_vertex, g->v_size-1);
		}
		if(destination_vertex > g->v_size) {
			printf("\tEnd point %u out or range. Graph vertex span: 0 to %u.\n", destination_vertex, g->v_size-1);
		}
	}
	if(!path_found) {
		printf("\tNo path from vertex %u to vertex %u found.\n", origin_vertex, destination_vertex);
	}
}

void LineupAdjacentVertexes(Graph *g, unsigned int origin_vertex, unsigned int *vertex_line, unsigned int *vertex_line_tail, unsigned int *visited_vertex_vector) {
	unsigned int i, level;

	level = visited_vertex_vector[origin_vertex] + 1;
	for(i = 0; i < g->v[origin_vertex].adjacent_vertex_array_last; i++) {
		if(visited_vertex_vector[g->v[origin_vertex].adjacent_vertex_array[i]] > level) {
			visited_vertex_vector[g->v[origin_vertex].adjacent_vertex_array[i]] = level;
			vertex_line[(*vertex_line_tail)++] = g->v[origin_vertex].adjacent_vertex_array[i];
		}
	}
}

void BFS(Graph *g, unsigned int origin_vertex, unsigned int destination_vertex) {
	unsigned int i;
	int path_found = 0;
	unsigned int *vertex_line, vertex_line_head, vertex_line_tail;
	unsigned int *visited_vertex_vector;

	printf("BFS: ");

	if(g->v_size < 1) {
		printf("Empty graph.\n");
		return;
	}

	if(origin_vertex < g->v_size && destination_vertex < g->v_size) {
		if(origin_vertex == destination_vertex) {
			printf("Trivial path found. origin and destination vertex are the same: %u.\n", origin_vertex);
			return;
		}

		vertex_line = (unsigned int *) malloc(sizeof(unsigned int) * g->v_size);
		visited_vertex_vector = (unsigned int *) malloc(sizeof(unsigned int) * g->v_size);

		for(i = 0; i < g->v_size; i++) {
			vertex_line[i] = visited_vertex_vector[i] = -1;
		}

		vertex_line[0] = origin_vertex;
		visited_vertex_vector[origin_vertex] = 0;

		for(vertex_line_head = 0, vertex_line_tail = 1; vertex_line_head < vertex_line_tail; vertex_line_head++) {
			if(vertex_line[vertex_line_head] == destination_vertex) {
				path_found = 1;
				printf("\tPath found: ");
				GraphTraceback(g, visited_vertex_vector, destination_vertex);
				return;
			}
			LineupAdjacentVertexes(g, vertex_line[vertex_line_head], vertex_line, &vertex_line_tail, visited_vertex_vector);
		}
		free(visited_vertex_vector);
		free(vertex_line);
	}
	else {
		if(origin_vertex > g->v_size) {
			printf("\n\tStarting point %u out or range. Graph vertex span: 0 to %u.\n", origin_vertex, g->v_size-1);
		}
		if(destination_vertex > g->v_size) {
			printf("\tEnd point %u out or range. Graph vertex span: 0 to %u.\n", destination_vertex, g->v_size-1);
		}
	}
	if(!path_found) {
		printf("\tNo path from vertex %u to vertex %u found.\n", origin_vertex, destination_vertex);
	}
}

int main(int argc, char **argv) {
	Graph *g;

	int max_edges = 512;
	int max_vertex = 64;
	int max_weight = 100;

	g = GraphCreate();

	int i;
	for(i = 0; i < max_edges; i++) {
		GraphEdgeAdd(g, EdgeCreateRand(max_vertex, max_weight));
	}
	//	DiGraphPrepare(g);
	GraphPrepare(g);

	GraphPrint(g);

	DFS(g, 14, 42);

	BFS(g, 14, 42);

	return 0;
}

//ToDo:	Add Union Find
//			Add Least cost path
//			...
